CC = @gcc
CFLAGS = $(CF) -Wall -Wextra -Wpedantic -Wwrite-strings -Wstack-usage=512 -Wfloat-equal -Waggregate-return -Winline
OUT = driver
DEPS = intersect.h
OBJ = driver.c intersect.c

$(OUT): $(OBJ)

debug: CFLAGS += -g
debug: $(OUT)

profile: LDFLAGS += -pg
profile: CLFAGS += -pg
profile: $(OUT)

clean:
	@rm -rf *.o $(OUT)

.PHONY: debug profile clean

