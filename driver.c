/*
 * TDQC5
 * Bruce Cain
 */

#include <stdio.h>
#include <stdlib.h>

#include "intersect.h"

int
main(
    int argc,
    char *argv[])
{
    size_t          file_on = 0;
    FILE           *in_file;
    int             cur_file = 0;
    uint8_t         keep_running = 1;
    Node           *root = NULL;

    if (argc < 3 && keep_running == 1)
    {
        puts("Use: <driver> [options] <file1> <file2> [<file3>, ...]");
        return -1;
    }

    while (++cur_file < argc)
    {
        if (strcmp(argv[cur_file], "-") == 0 && cur_file == 1)
        {
            in_file = stdin;
        }
        else
        {
            in_file = fopen(argv[cur_file], "r");
        }

        if (in_file == NULL)
        {
            printf("%s - does not exist\n", argv[cur_file]);
            file_on++;
            continue;
        }

        keep_running = read_file(&root, in_file, file_on++);
    }

    if (keep_running == 1)
    {
        print_tree(root, argc - 1);
    }

    destroy_tree(root);
    return 0;
}
