/*
 * Author: Bruce Cain
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <unistd.h>

#include "intersect.h"

/**
 * Global used to catch a Ctrl-C to gracefully stop the program
 */
static uint8_t  keep_running = 1;

/**
 * A struct that holds the data for a Node.
 */
typedef struct Node
{
    /*@{ */
    char           *word;   /**< Word from file*/
    uint32_t        count;  /**< How many times found in file */
    struct Node    *left;   /**< Left child */
    struct Node    *right;  /**< Right child */
    struct Node    *parent; /**< Parent node */
    /*@} */
} Node;

static void
handler(
    int sig)
{
    printf("Exit on %d\n", sig);
    keep_running = 0;
}

/**
 * @func rotate_left
 * @brief Rotates Node_x to the left.
 * @param node_x The node to be rotated left.
 *
 * https://en.wikipedia.org/wiki/Splay_tree 
 */
static void
rotate_left(
    Node * node_x)
{
    Node           *node_y = node_x->right;

    if (node_y != NULL)
    {
        node_x->right = node_y->left;
        if (node_y->left != NULL)
        {
            node_y->left->parent = node_x;
        }

        node_y->left = node_x;
        node_y->parent = node_x->parent;
    }

    if (node_x->parent != NULL)
    {
        if (node_x->parent->left == node_x)
        {
            node_x->parent->left = node_y;
        }
        else
        {
            node_x->parent->right = node_y;
        }
    }

    node_x->parent = node_y;
}

/*
 * @func rotate_right
 * @brief Rotates the node to the right.
 * @param node_x The node being rotated to the right.
 *
 * https://en.wikipedia.org/wiki/Splay_tree 
 */
static void
rotate_right(
    Node * node_x)
{
    Node           *node_y = node_x->left;

    if (node_y != NULL)
    {
        node_x->left = node_y->right;
        if (node_y->right != NULL)
        {
            node_y->right->parent = node_x;
        }

        node_y->right = node_x;
        node_y->parent = node_x->parent;
    }

    if (node_x->parent != NULL)
    {
        if (node_x->parent->left == node_x)
        {
            node_x->parent->left = node_y;
        }
        else
        {
            node_x->parent->right = node_y;
        }
    }

    node_x->parent = node_y;
}

/*
 * @func splay
 * @brief Move the node to root.
 * @param node The node being moved to root.
 * @return Node * The pointer to the new root.
 *
 * https://en.wikipedia.org/wiki/Splay_tree 
 * */
static Node    *
splay(
    Node * node)
{
    while (node->parent != NULL)
    {
        if (node->parent->parent == NULL)
        {
            if (node->parent->left == node)
            {
                rotate_right(node->parent);
            }
            else
            {
                rotate_left(node->parent);
            }
        }
        else if (node->parent->left == node &&
                 node->parent->parent->left == node->parent)
        {
            rotate_right(node->parent->parent);
            rotate_right(node->parent);
        }
        else if (node->parent->right == node &&
                 node->parent->parent->right == node->parent)
        {
            rotate_left(node->parent->parent);
            rotate_left(node->parent);
        }
        else if (node->parent->left == node &&
                 node->parent->parent->right == node->parent)
        {
            rotate_right(node->parent);
            rotate_left(node->parent);
        }
        else
        {
            rotate_left(node->parent);
            rotate_right(node->parent);
        }
    }

    return node;
}

/**
 * @func check_punct
 * @brief Checks if the word is only punctuation.
 * @param word The word being checked.
 * @return 1 if the word is only punctuation and 0 if it contains other
 *  characters.
 */
static int
check_punct(
    char *word)
{
    int             return_val;

    for (size_t i = 0; i < strlen(word); i++)
    {
        if (ispunct(word[i]))
        {
            return_val = 1;
        }
        else
        {
            return_val = 0;
            break;
        }
    }

    return return_val;
}

/**
 * @func create_node
 * @brief Create a node in the tree.
 * @param word The word to be kept in the node.
 * @return The new node created.
 */
static Node    *
create_node(
    char *word)
{
    Node           *new_node = calloc(1, sizeof(*new_node));

    if (!new_node)
    {
        printf("New node didn't malloc\n");
        exit(1);
    }

    new_node->word = calloc(strlen(word) + 1, sizeof(char));

    if (!(new_node->word))
    {
        free(new_node);
        printf("New node word didn't malloc\n");
        exit(1);
    }

    strcpy(new_node->word, word);
    new_node->count = 1;
    new_node->left = NULL;
    new_node->right = NULL;
    new_node->parent = NULL;

    return new_node;
}

/**
 * @func search_tree
 * @brief Searchs tree for a given word.
 * @param root A pointer to root node.
 * @param word Is the word being searched for.
 * @param file_on Is the file currently on.
 * @return Node * Is the new root node.
 */
static Node    *
search_tree(
    Node ** root,
    char *word,
    uint32_t file_on)
{
    Node           *tmp_root = NULL;

    if (word == '\0')
    {
        return tmp_root;
    }

    if ((*root) == NULL)
    {
        return tmp_root;
    }

    int             compare_result = strcasecmp((*root)->word, word);

    if (compare_result == 0)
    {
        if (file_on == (*root)->count)
        {
            (*root)->count++;
        }

        return splay(*root);
    }
    else if (compare_result > 0)
    {
        if ((*root)->left)
        {
            tmp_root = search_tree((&(*root)->left), word, file_on);
        }
    }
    else
    {
        if ((*root)->right)
        {
            tmp_root = search_tree((&(*root)->right), word, file_on);
        }
    }

    return tmp_root;
}

/**
 * @func insert_node
 * @brief Inserts a new node into the tree.
 * @param root A point to the root node.
 * @param word The word to be inserted into the tree.
 * @return Node * Is the new root node
 */
static Node    *
insert_node(
    Node ** root,
    char *word)
{
    Node           *tmp_root = NULL;

    if (word == '\0')
    {
        return tmp_root;
    }

    if ((*root) == NULL)
    {
        *root = create_node(word);
        return tmp_root;
    }

    int             compare_result = strcasecmp((*root)->word, word);

    if (compare_result == 0)
    {
        return splay(*root);
    }

    compare_result = strcmp((*root)->word, word);

    if (compare_result > 0)
    {
        if ((*root)->left)
        {
            tmp_root = insert_node((&(*root)->left), word);
        }
        else
        {
            (*root)->left = create_node(word);
            (*root)->left->parent = (*root);
            return splay(*root);
        }
    }
    else
    {
        if ((*root)->right)
        {
            tmp_root = insert_node((&(*root)->right), word);
        }
        else
        {
            (*root)->right = create_node(word);
            (*root)->right->parent = (*root);
            return splay(*root);
        }
    }

    return tmp_root;
}

void
destroy_tree(
    Node * root)
{
    if (root == NULL)
    {
        return;
    }

    destroy_tree(root->left);
    destroy_tree(root->right);

    free(root->word);
    free(root);
}

void
print_tree(
    Node * root,
    uint32_t num_files)
{
    if (root == NULL)
    {
        return;
    }

    print_tree(root->left, num_files);

    if (root->count == num_files)
    {
        printf("%s\n", root->word);
    }

    print_tree(root->right, num_files);
}


uint8_t
read_file(
    Node ** root,
    FILE * stream,
    size_t file_on)
{
    char           *line = NULL, *word = NULL;
    char            sep[11] = " \t\v\r\f\n";
    size_t          size = 0;
    Node           *tmp_root = NULL;
    struct sigaction *sig_catcher = calloc(1, sizeof(*sig_catcher));

    sig_catcher->sa_handler = handler;

    sigaction(SIGINT, sig_catcher, NULL);

    while (getline(&line, &size, stream) != -1 && keep_running == 1)
    {
        for (word = strtok(line, sep); word; word = strtok(NULL, sep))
        {
            if (strlen(word) > 256)
            {
                continue;
            }

            if (!check_punct(word))
            {
                if (file_on == 0)
                {
                    tmp_root = insert_node(root, word);
                }
                else
                {
                    tmp_root = search_tree(root, word, file_on);
                }

                if (tmp_root != NULL)
                {
                    *root = tmp_root;
                }
            }
        }

    }

    fclose(stream);
    free(line);
    free(sig_catcher);

    return keep_running;
}
