/*
 * Authoer: Bruce Cain
 */

#ifndef INTERSECT_H
#define INTERSECT_H

#include <string.h>
#include <stdint.h>

/*
 * Enable users to create Node types.
 */
typedef struct Node Node;

/**
 * @func destroy_tree
 * @brief Free's the allocated binary tree.
 * @param root Is the root node of the tree.
 */
void            destroy_tree(
    Node * root);

/**
 * @func print_tree
 * @brief Prints the tree in order.
 * @param root The start of the tree.
 * @param num_files The total number of files read in to make sure the word
 *  was found in every file.
 */
void            print_tree(
    Node * root,
    uint32_t num_files);

/**
 * @func read_file
 * @brief Reads and parses and file stream.
 * @param root A pointer to the root node.
 * @param stream The file stream pointer.
 * @param file_on Is the file currently on.
 * @return A 0 if Ctrl+C is caught else 1.
 */
uint8_t         read_file(
    Node ** root,
    FILE * stream,
    size_t file_on);
#endif
